import React, {Component} from 'react';
import PropTypes from 'prop-types'
import classNames from 'classnames';

/**
 */
export default class Item extends Component {
    /**
     * constructor
     * @param {Object} props
     */
    constructor(props) {
        super(props);
        this.state={
            checked: this.props.checked,
        }
    }

    componentWillReceiveProps(nextProps){
        let i =5;
        if(nextProps.checked !== this.props.checked){
            this.setState({checked:nextProps.checked});
        }
    }

    itemCliked(){
        this.setState({checked: !this.state.checked})
    }

    /**
     * render
     * @return {ReactElement} markup
     */
    render() {
        return ( // onClick={this.props.onclick.bind(this.props.id)}
            <div className="checkItem" onClick={this.itemCliked.bind(this)}>
                <input className="myCheckBox" type="checkbox" checked={this.state.checked}  />
                <p  className="itemName">{this.props.name}</p>
            </div>
        );
    }
}

/**
 * propTypes
 */
Item.PropTypes = {
    language: PropTypes.object,
    name: PropTypes.string.isRequired,
    id: PropTypes.number.isRequired,
    checked: PropTypes.bool,
    onClick: PropTypes.func,
};
