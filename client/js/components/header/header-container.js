import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import Header from './header';

const mapStateToProps = (state) => {
    return {
        language: state.language,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
    };
};

const headerContainer = connect(mapStateToProps, mapDispatchToProps)(Header);
export default headerContainer;
