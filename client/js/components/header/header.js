import React, {Component, PropTypes} from 'react';
import {Link} from 'react-router';
import {forEach} from 'lodash';

import '../../../css/header.css';

export default class Header extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <header>
                <div>"this is the header"</div>
            </header>
        );
    }

}

Header.propTypes = {
    language: PropTypes.object,
};
