import {connect} from 'react-redux';
import {loadLanguage} from '../../actions/login-actions';
import {bindActionCreators} from 'redux';
import Main from './main';

const mapStateToProps = (state) => {
    return {
        language: state.language,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        loadLanguage: bindActionCreators(loadLanguage, dispatch)
    };
};

const mainContainer = connect(mapStateToProps, mapDispatchToProps)(Main);
export default mainContainer;
