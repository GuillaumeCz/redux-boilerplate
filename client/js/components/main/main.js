import React, {Component, PropTypes} from 'react';
import Header from '../header/header-container';
import language from '../../../assets/text/languages';

import '../../../css/animations.css';
import '../../../css/main.css';
import '../../../css/shadows.css';

export default class Main extends Component {
    constructor(props) {
        super(props);
        this.props.loadLanguage(language.FR);
    }

    render() {
        return (
            <div className="container">
                <Header />
                <div className="main">
                    {this.props.main ? this.props.main : ''}
                </div>
            </div>
        );
    }
}


/**
* @property {Object} [language] - This object contain all the text translations.
*/
Main.propTypes = {
    language: PropTypes.object,
}