import {connect} from 'react-redux';
import Reception from './reception';

// all states or functions found here can be accessed through this.props.yourStateOrFunction in the reception.js class
const mapStateToProps = (state) => {
    return {
        language: state.language,
        startTime: state.reception.startTime,
        endTime: state.reception.endTime,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {}
};

const receptionContainer = connect(mapStateToProps, mapDispatchToProps)(Reception);
export default receptionContainer
