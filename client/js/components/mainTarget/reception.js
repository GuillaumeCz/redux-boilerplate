import React, {Component, PropTypes} from 'react';
import classnames from 'classnames';
import {concat} from 'lodash';

export default class Reception extends Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() {
    }

    render(){
        return (
            <div  className="container">
                hello world
            </div>
        );
    }
}

/**
* propTypes
* @property {Object} [language] - This object contain all the text translations.
* @property {array} [analyseurs] - This is a list of all the analyseurs with theres childs and analyses .
*/

Reception.propTypes = {
    language: PropTypes.object.isRequired,
    analyseurs: PropTypes.array,
};

