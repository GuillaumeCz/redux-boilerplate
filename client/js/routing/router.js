import React, {Component, PropTypes} from 'react';
import {Router, Route, IndexRoute} from 'react-router';
import {isEmpty} from 'lodash';
import Main from '../components/main/main-container';
import Reception from '../components/mainTarget/Reception-container';

export default class RouterContainer extends Component {

    constructor(props) {
        super(props);
/*        this.requireSite = this.requireSite.bind(this);
        this.requireAuth = this.requireAuth.bind(this);*/
        this.routes = (
            <Router history={this.props.history}>

                <Route path="/" component={Main} >
                    <IndexRoute components={{main: Reception}}/>
                    <Route path="Reception" components={{main: Reception}}/>

{/*                <Route path="/siteconfiguration" component={SiteConfigurationContainer}/>
                <Route path="/login" component={LoginContainer} onEnter={this.requireSite}/>
                <Route path="/" component={Main} onEnter={this.requireAuth}>*/}
                </Route>
            </Router>
        );
    }

/*    requireSite(nextState, replace) {
        if (!this.props.store.getState().inputData.choosen.site.id && isEmpty(localStorage.choosen)) {
            replace({
                pathname: '/defaultUrl'
            });
        }
    }

    requireAuth(nextState, replace) {
        this.requireSite(nextState, replace);
        if (!this.props.store.getState().inputData.logInfo.connected && isEmpty(localStorage.token) && isEmpty(localStorage.idUser)) {
            replace({
                pathname: '/login'
            });
        } else if (nextState.location.pathname === '/') {
            replace({
                pathname: '/defaultUrl'
            });
        }
    }*/

    render() {
        return this.routes;
    }
}

RouterContainer.propTypes = {
    history: PropTypes.object.isRequired
};
