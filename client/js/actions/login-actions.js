import superagent from 'superagent';
import {apiUrls} from '../appConfig';
import {merge, concat, sortBy} from 'lodash';
import {bindActionCreators} from 'redux';


export const LOAD_LANGUAGE = 'LOAD_LANGUAGE';
export function loadLanguage(language) {
    return (dispatch) => {
        dispatch({type: LOAD_LANGUAGE, language: language});
    }
}
