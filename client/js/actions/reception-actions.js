//import {importedUtil} from '../util.js';

export const UPDATE_TIME_BOUNDARIES = 'UPDATE_TIME_BOUNDARIES';
export function updateTimeBoundaries(startTime, endTime) {
    return (dispatch) => {
        dispatch({type: UPDATE_TIME_BOUNDARIES,  startTime, endTime}); // or {type: UPDATE_TIME_BOUNDARIES, endTime: endTime, endTime: endTime}
    }
}


