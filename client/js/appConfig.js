import {useRouterHistory} from 'react-router';
import {createHistory} from 'history';

export const apiUrls = {
    login: '/api/login',
};

// link an icon name to the corresponding font character
export const iconeTranslator = {
    routeur:"q",
    analyseur:"a",
    tampon:"y",
    archive:"h",
    cible:"w",
    sortie:"q",
    module:"z",
    analyse:"x",
    route:"t",
    urgent:"b",
    burger:"m",
    maskOff:"v",
    maskOn:"p",
    maskAutoOn:"t",
    maskAutoOff:"p",
    supervision:"n",
    journal:"l",
    initTitle:"s",
    routesManager:"t",
    connectionsManager:"w",
    search:"u",
    warning:"k",
    arrow:"j",
    userInfo:"g",
    searchBarCode:"f",
    ambulance:"b",
    tubeGetsion:"i",
    rochLogo:"A",
    checked:"D",
    minus:"E",
    circle:"F",
    circleChecked:"G",
    plus:"H",
    cross:"I",
    tube:"J",
    triangle:"K",
    doubleArrow:"L",
    lock:"M",
    refresh:"N",
    logout:"O",
    forwardAna:"P",
    envoiTube:"Q",
    bigArrow:"R",
    lab:"S",
    transfer:"T",
    connected:"U",
    notConnected:"V",
    paramMPTA:"W",
};
// Custom browser history avec prefix
export const browserHistory = useRouterHistory(createHistory)({
    basename: ''
});
