import React from 'react';
import ReactDOM from 'react-dom';
import {createStore, applyMiddleware, compose} from 'redux';
import {Provider} from 'react-redux';
import {syncHistoryWithStore, routerMiddleware} from 'react-router-redux';
import thunk from 'redux-thunk';
import reducers from './reducers';
import {browserHistory} from './appConfig';
import RouterContainer from './routing/router';

import '../css/app.css';

let defaultStore = {
    language: {},
    routing:{},
    reception:{
        startTime:0,
        endTime:0,
    }
};

const middlewares = [routerMiddleware(browserHistory), thunk];

const store = createStore(
    reducers,
    defaultStore,
    compose(
        applyMiddleware(...middlewares),
        window.devToolsExtension ? window.devToolsExtension() : f => f
    )
);

const domElement = document.getElementById('main');

const history = syncHistoryWithStore(browserHistory, store);

const routing =
    <Provider store={store}>
        <RouterContainer history={history} store={store}/>
    </Provider>;

ReactDOM.render(routing, domElement);
