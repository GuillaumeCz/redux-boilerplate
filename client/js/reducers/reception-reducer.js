import {findIndex, concat} from 'lodash';
import {UPDATE_TIME_BOUNDARIES} from '../actions/reception-actions';

const receptionReducer = (state = {}, action) => {
    switch (action.type) {

        case UPDATE_TIME_BOUNDARIES : {
            let newState = {...state};
            newState.startTime = action.startTime;
            newState.endTime = action.endTime;
            return newState;
        }

        default:
            return state;
    }
};

export default function (state = {}, action) {
    return receptionReducer(state, action);
}
