import {combineReducers} from "redux";

import languageReducer from './language-reducer';
import routerReducer from './router-reducer';
import receptionReducer from './reception-reducer';

export default combineReducers({
    language: languageReducer,
    routing: routerReducer,
    reception: receptionReducer,
});
