let language = {};
language.EN = {

};
language.FR = {
    thisFilLanguage: "FR",
    dateFrom: "Du",
    dateTo: "Au",
    validate: "valider",
    control:"Contôle",
    analyse:"Analyse",
    start:"Début",
    end:"Fin",
    target:"Cible",
    conf:"Int.Conf",
    theory:"CV Théorique",
    graphMoy:"Moy. Graph",
    graphET:"E.T. Graph",
    graphCV:"CV Graph",
    checkAll:"Appliquer au sous elements",
};

module.exports = language;
