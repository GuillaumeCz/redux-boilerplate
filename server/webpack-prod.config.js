const path = require('path');
const webpack = require('webpack');
var config = require('./config.json');
var EncodingPlugin = require('webpack-encoding-plugin');

var root = path.join(__dirname, config.root);

module.exports = {
    devtool: 'cheap-module-source-map',

    entry: [
        path.join(root, '/js/app')
    ],

    output: {
        path: path.join(__dirname, '/dist/'),
        filename: 'bundle.js',
        publicPath:  './dist/'
    },

    plugins: [
        new webpack.optimize.OccurenceOrderPlugin(),
        new webpack.DefinePlugin({'process.env': {'NODE_ENV': JSON.stringify('production')}}),
        new webpack.ProvidePlugin({$: "jquery", jQuery: "jquery"}),
        new EncodingPlugin({encoding: 'utf-8'})
    ],
    
    resolve: {
        extensions: ['', '.js', '.jsx']
    },

    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                loaders: ['react-hot', 'babel'],
                include: path.join(root, '/js')
            },
            {
                test: /\.css$/,
                loader: 'style-loader!css-loader'
            },
            {
                test: /\.png$/,
                loader: "url-loader?limit=100000"
            },
            {
                test: /\.jpg$/,
                loader: "file-loader"
            },
            {
                test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url?limit=10000&mimetype=application/font-woff'
            },
            {
                test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url?limit=10000&mimetype=application/octet-stream'
            },
            {
                test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'file'
            },
            {
                test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url?limit=10000&mimetype=image/svg+xml'
            }
        ]
    }
};
